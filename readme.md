# Asset

A laravel tool that extends Illuminate\html functionality, to better provide default functionality for my project structure.

In the config/app.php, add this under the providers array

```
App\Providers\AssetServiceProvider::class,
```

In the config/app.php, add this under the aliases array

```
'Asset'      => App\Http\Facades\AssetFacade::class,
```