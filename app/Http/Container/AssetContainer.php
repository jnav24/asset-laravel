<?php

namespace App\Http\Containers;

use Illuminate\Html\HtmlBuilder;

class AssetContainer extends HtmlBuilder
{
    public $css = 'css/';
    public $img = 'images/';
    public $js = 'js/';

    public function img($url, $alt = null, $attributes = array())
    {
        $path = $this->img . $url;

        if ($this->url->isValidUrl($url))
        {
            $path = $url;
        }

        return $this->image($path, $alt, $attributes);
    }

    public function menu($menu, $attributes = [])
    {
        return $this->ul($menu, $attributes);
    }

    public function css($url, $attributes = [])
    {
        $path = $this->css . $url;

        if ($this->url->isValidUrl($url))
        {
            $path = $url;
        }

        return $this->style($path, $attributes);
    }

    public function js($url, $attributes = [])
    {
        $path = $this->js . $url;

        if ($this->url->isValidUrl($url))
        {
            $path = $url;
        }

        return $this->script($path, $attributes);
    }

    public function imgLink($url)
    {
        return $this->url->asset($this->img . $url);
    }
}