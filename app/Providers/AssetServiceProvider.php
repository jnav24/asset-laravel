<?php

namespace App\Providers;

use App\Http\Containers\AssetContainer;
use Illuminate\Support\ServiceProvider;

class AssetServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerAssetContainer();
    }

    protected function registerAssetContainer()
    {
        $this->app->bindShared('asset', function($app) {
            return new AssetContainer($app['url']);
        });
    }
}